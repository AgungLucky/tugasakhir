﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Data_Input : MonoBehaviour {

    // Masukkan jawaban
    public string URL;
    //public string inputJawaban;
    public InputField vjawab;
    public InputField INilaiX;
    public InputField InilaiZ;
    public Text IdSoal;
    public Text nis;
    public Text waktu;

    //string creatURL = "";
    //string globalURL = koneksi.instance.global_URL;

    //public WWWForm inputNilai;

    IEnumerator kirimJawaban() {
        string globalNis = Login.instance.global_NIS;
        string globalURL = koneksi.instance.global_URL;
        string URL = "http://"+globalURL+"/mesin_bubut/insert_jawaban.php";
        WWWForm php_jwb;
        php_jwb = new WWWForm();
        php_jwb.AddField("jawabanPost", vjawab.text);
        php_jwb.AddField("nis", globalNis.ToString());
        php_jwb.AddField("idSoalPost", IdSoal.text);
        php_jwb.AddField("waktuPost",waktu.text);
        //Debug.Log(IdSoal);
        WWW URLjawaban = new WWW(URL, php_jwb);
        yield return URLjawaban;
    }

    public void buttonInsert() {
        StartCoroutine(kirimJawaban());
        vjawab.text = " " ;
        Date();
    }

    public void Date()
    {
        waktu.text = System.DateTime.Now.ToString("HH:mm / dd-MM-yyyy");

    }


}
