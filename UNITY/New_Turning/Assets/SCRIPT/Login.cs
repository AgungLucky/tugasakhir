﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Login : MonoBehaviour {
    public string URL;
    public InputField txtuser;
    public InputField txtpass;
    public Text txtststus;
    string vText;

    public static Login instance { get; private set; }
    public string global_NIS;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


    IEnumerator PostLogin() {

        string globalURL = koneksi.instance.global_URL;
        string URL = "http://"+globalURL+"/mesin_bubut/login.php";
        Debug.Log(koneksi.instance.global_URL);
        WWWForm php_form;
        php_form = new WWWForm();
        php_form.AddField("nama",txtuser.text);
        php_form.AddField("nis",txtpass.text);
        WWW login = new WWW(URL, php_form);
        yield return login;
        string status = ambil_value(login.text, "status");
        string nis = ambil_value(login.text, "nis :");

        Debug.Log(status);
        Debug.Log(nis);
       
        if (status != " " && status != "INVALID")
        {
            //berhasil
            Login.instance.global_NIS = nis;
            //setnis
            Application.LoadLevel("Halaman_utama");
        }
        else {
            txtststus.text = "LOGIN Gagal";
        }//endif
    }

    string ambil_value(string data, string index)
    {
        string soal_value = data.Substring(data.IndexOf(index) + index.Length);
        if (soal_value.Contains("|")) soal_value = soal_value.Remove(soal_value.IndexOf("|"));
        return soal_value;
    }

    public void FusionLogin() {
        StartCoroutine(PostLogin());
    }

	
}
