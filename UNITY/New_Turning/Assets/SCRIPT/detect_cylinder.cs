﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detect_cylinder : MonoBehaviour {

    public GameObject _cube;
    private Transform _cylinder;

    private void Start()
    {
        _cylinder = GetComponent<Transform>();
    }

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "cube")
        {
            Debug.Log("Scale x, y");

            this.gameObject.transform.localScale = new Vector3(transform.localScale.x - 0.01f, 0.01f, transform.localScale.z - 0.01f);
        }
    }
}
