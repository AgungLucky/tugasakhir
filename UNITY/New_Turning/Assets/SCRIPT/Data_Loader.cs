﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Data_Loader : MonoBehaviour {
    

    // Ambil Soal
    public string[] AmbilSoal;
    public int jumlah_soal;
    public Text vSoal;
    public Text vjawab1;
    public Text vjawab2;
    public Text vjawab3;
    public Text vjawab4;
    public Text vid;
    public Text vnis;
    public int index_soal = -1;
    public Text waktu;

	IEnumerator Start () {
        //string globalNIS = Login.instance.global_NIS;
        vnis.text = Login.instance.global_NIS;
        string globalURL = koneksi.instance.global_URL;
        string URL = "http://"+globalURL+"/mesin_bubut/ambil_soal.php";
        WWW ambil_soal = new WWW(URL);
        yield return ambil_soal;
        string ambil_soal_string = ambil_soal.text;
        print (ambil_soal_string);
        AmbilSoal = ambil_soal_string.Split(';');
        jumlah_soal = AmbilSoal.Length;
        //Debug.Log(jumlah_soal);
        print(ambil_soal_value(AmbilSoal[0], "jawaban2 :"));
        Date();

    }

    string ambil_soal_value(string data, string index) {
        string soal_value = data.Substring(data.IndexOf(index) + index.Length);
        if(soal_value.Contains("|")) soal_value = soal_value.Remove(soal_value.IndexOf("|"));
        return soal_value;
    }

    public void nextSoal() {
        if (index_soal < jumlah_soal)
        {
            index_soal++;
            vid.text = ambil_soal_value(AmbilSoal[index_soal], "id :");
            vSoal.text = ambil_soal_value(AmbilSoal[index_soal], "soal :");
            vjawab1.text = ambil_soal_value(AmbilSoal[index_soal], "jawaban1 :");
            vjawab2.text = ambil_soal_value(AmbilSoal[index_soal], "jawaban2 :");
            vjawab3.text = ambil_soal_value(AmbilSoal[index_soal], "jawaban3 :");
            vjawab4.text = ambil_soal_value(AmbilSoal[index_soal], "jawaban4 :");
            
        } else
        {
            //beres
        }

    }


    public void preSoal()
    {
        if (index_soal != 0 )
        {
            index_soal--;
            vid.text = ambil_soal_value(AmbilSoal[index_soal], "id :");
            vSoal.text = ambil_soal_value(AmbilSoal[index_soal], "soal :");
            vjawab1.text = ambil_soal_value(AmbilSoal[index_soal], "jawaban1 :");
            vjawab2.text = ambil_soal_value(AmbilSoal[index_soal], "jawaban2 :");
            vjawab3.text = ambil_soal_value(AmbilSoal[index_soal], "jawaban3 :");
            vjawab4.text = ambil_soal_value(AmbilSoal[index_soal], "jawaban4 :");
            
        }
        else
        {
            //beres
        }

    }

    public void Date()
    {
        waktu.text = System.DateTime.Now.ToString("HH:mm / dd-MM-yyyy");
        
    }


}//endclass
