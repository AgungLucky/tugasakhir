﻿using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Contoh_Otomatis : MonoBehaviour {

    // masukkan nilai X,Z       
    public InputField nilaiX;
    public InputField nilaiY;
    public Text tHasil;
    //
    public float _speed = 5f;
    public string nilx;
    public GameObject cube;
    public GameObject box;
    public Texture2D oke;
    public float temp;
    public bool klik;
    public float delay_ = -10f;
    public Transform target;
    public string teks="";
    
    void Start()
    {
        
    }

    IEnumerator jarak(float x)
    {
        transform.Translate(Vector3.forward * _speed * Time.deltaTime);
        yield return new WaitForSeconds(x/5);
        klik = false;
    }


    void Update()
    {
        if (klik == true)
        {
            ambilNilai();
            float seq;
            seq = temp - Time.deltaTime;

            if (temp != 0)
            {
                //Debug.Log(temp);
                float step = _speed * Time.deltaTime;
                StartCoroutine(jarak(temp));
                
            }


            else
            {
                //transform.Translate(Vector3(0,_speed,0) * Time.deltaTime);
                transform.Translate(0f, 0f, 0f * Time.deltaTime);
            }
        }
        else
        { 
            transform.Translate(0f, 0f, 0f);
            //Debug.Log("gagal");
        }

    }



    public void ambilNilai()
    {
        nilx = nilaiX.text;
        //float fnilaix = float.Parse(nilx);
        temp = float.Parse(nilx);
        //Debug.Log(fnilaix);
    }


    public void tampil()
    {
        klik = true;
    }

    
}

