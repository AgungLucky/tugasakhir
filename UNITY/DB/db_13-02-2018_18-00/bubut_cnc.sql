-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2018 at 11:59 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bubut_cnc`
--

-- --------------------------------------------------------

--
-- Table structure for table `det_jwb_siswa`
--

CREATE TABLE `det_jwb_siswa` (
  `id_det_jwb_siswa` int(10) NOT NULL,
  `id_jwb_siswa` int(5) NOT NULL,
  `id_soal` int(5) NOT NULL,
  `id_jawaban` int(5) NOT NULL,
  `jwb_essay` text NOT NULL,
  `bobot_essay` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `nip` int(30) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`nip`, `nama`) VALUES
(1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE `jawaban` (
  `id_jawaban` int(5) NOT NULL,
  `id_soal` int(5) DEFAULT NULL,
  `jawaban_` varchar(75) NOT NULL,
  `bobot` int(3) DEFAULT NULL,
  `nis` int(10) DEFAULT NULL,
  `waktu` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jawaban`
--

INSERT INTO `jawaban` (`id_jawaban`, `id_soal`, `jawaban_`, `bobot`, `nis`, `waktu`) VALUES
(48, 4, 'jarak yang dilintasi 10 m/mm', NULL, 10113355, '13:12 / 07-02-2');

-- --------------------------------------------------------

--
-- Table structure for table `jawab_siswa`
--

CREATE TABLE `jawab_siswa` (
  `id_jwb_siswa` int(5) NOT NULL,
  `nis` int(10) NOT NULL,
  `nilai` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nis` int(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `kelas` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nis`, `nama`, `kelas`) VALUES
(1141143, 'Abdul Razak', 'TPM-1'),
(1152098, 'Aan Setiawan', 'TPM-1'),
(1152099, 'Adji Mustopa', 'TPM-1'),
(1152100, 'Aldi Nur Alim', 'TPM-1'),
(1152101, 'Andika Abdillah Suherman', 'TPM-1'),
(1152102, 'Arsyi Ansori', 'TPM-1'),
(1152103, 'Asep wahyudi', 'TPM-1'),
(1152107, 'Dhiyan Hidayat Sutisna', 'TPM-1'),
(1152108, 'Dimas Saputro', 'TPM-1'),
(1152109, 'Ega koswara', 'TPM-1'),
(1152110, 'Fajar Miftah Rohman', 'TPM-1'),
(1152111, 'Fauzi Nurul Azmi', 'TPM-1'),
(1152112, 'Hani Maryam', 'TPM-1'),
(1152113, 'Hanti Pebrianti', 'TPM-1'),
(1152115, 'Ilham Nur Fadhilah', 'TPM-1'),
(1152116, 'Juniar Hadi Prayoga', 'TPM-1'),
(1152117, 'M. Ramlan Jalil Jaelani', 'TPM-1'),
(1152118, 'Mochamad Raihansyah', 'TPM-1'),
(1152119, 'Mohamad Dafa Abdan Padilla P', 'TPM-1'),
(1152120, 'Muhamad Nizar Wahidul Yaqin', 'TPM-1'),
(1152121, 'Muhammad Firman Zainul F', 'TPM-1'),
(1152124, 'Ramdani Tri Sanjaya', 'TPM-1'),
(1152125, 'Rico Langgeng Wahyudi', 'TPM-1'),
(1152126, 'Rifki Taufik', 'TPM-1'),
(1152127, 'Rizal Faturrahman Sidik', 'TPM-1'),
(1152128, 'Rizky Nurul Huda', 'TPM-1'),
(1152129, 'Robby Topan Alfarisyi', 'TPM-1'),
(1152131, 'Sandika Aditya', 'TPM-1'),
(1152132, 'Sopan Saputra', 'TPM-1'),
(1152133, 'Tubagus Rohman', 'TPM-1'),
(1152136, 'Aep', 'TPM-2'),
(1152138, 'Ara Sukmana', 'TPM-2'),
(1152139, 'Badru Zaman Kamil', 'TPM-2'),
(1152140, 'Danang Tri Sukmono', 'TPM-2'),
(1152141, 'Daniel Pardingotan Sinurat', 'TPM-2'),
(1152142, 'Denar Saepul', 'TPM-2'),
(1152143, 'Dicky Maulana', 'TPM-2'),
(1152144, 'Dio Muhammad Taufiq', 'TPM-2'),
(1152145, 'Eko Mulyono Yusuf', 'TPM-2'),
(1152146, 'Fajar Sidiq', 'TPM-2'),
(1152147, 'Febi Sumantri', 'TPM-2'),
(1152148, 'Hilman Ahmad Fathoni', 'TPM-2'),
(1152150, 'Irfan Maulana Anzani', 'TPM-2'),
(1152151, 'Kemal Gunawan', 'TPM-2'),
(1152152, 'Muhamad Sumpena', 'TPM-1'),
(1152153, 'Mochamad Rizal Mardiansyah', 'TPM-2'),
(1152154, 'Mohamad Rizal Baehaki', 'TPM-2'),
(1152156, 'Muhammad Rizal Fadilah', 'TPM-2'),
(1152157, 'Naufal Ramdhani Praja', 'TPM-2'),
(1152159, 'Redy Tridiana', 'TPM-2'),
(1152160, 'Reza Fauzi', 'TPM-2'),
(1152161, 'Ridwan Firmansyah', 'TPM-2'),
(1152162, 'Rio Agustian', 'TPM-2'),
(1152163, 'Rizal Sumpena', 'TPM-2'),
(1152164, 'Rizqi Pratama Putra', 'TPM-2'),
(1152167, 'Subhan Efendi', 'TPM-2'),
(1152169, 'Yohannes Hendra Banjarnahor', 'TPM-2'),
(1152170, 'Adi Sumarsono', 'TPM-3'),
(1152171, 'Agil Muhamad Yusup', 'TPM-3'),
(1152172, 'Aldi Prayatno', 'TPM-3'),
(1152173, 'Ari Septiana', 'TPM-3'),
(1152174, 'Arya Herdimansyah', 'TPM-3'),
(1152175, 'Bagus Wicaksono', 'TPM-3'),
(1152178, 'Denny Setiawan', 'TPM-3'),
(1152179, 'Didit Aditya', 'TPM-3'),
(1152181, 'Erald Firmansyah', 'TPM-3'),
(1152182, 'Farhan Iswan Nurfakhruddin', 'TPM-3'),
(1152183, 'Gilang Budi Setiawan', 'TPM-3'),
(1152185, 'Husaena Muhammad Nurdin', 'TPM-3'),
(1152186, 'Ilham Yusril Maulana', 'TPM-3'),
(1152187, 'Ivan Sonjaya', 'TPM-3'),
(1152189, 'Marchelino Prayoga', 'TPM-3'),
(1152191, 'Mohammad Rezza Faesol', 'TPM-3'),
(1152192, 'Muhamad Rizky Rosady', 'TPM-3'),
(1152193, 'Muhammad Rizki Nurrahman', 'TPM-3'),
(1152195, 'Rahman Nurdin', 'TPM-3'),
(1152197, 'Rifal Septian Irawan', 'TPM-3'),
(1152198, 'Riski Ramdani', 'TPM-3'),
(1152200, 'Rizski Pajar Maulana', 'TPM-3'),
(1152201, 'Rohimatulloh', 'TPM-3'),
(1152203, 'Sendy Rizkia Irpansyah', 'TPM-3'),
(1152204, 'Sulchan Kamal', 'TPM-3'),
(1152206, 'Yuphi Roel Lamsyah', 'TPM-3'),
(1152504, 'Farhan Arif Fadillah', 'TPM-2'),
(10113355, 'agung', 'TPM-1');

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id_soal` int(5) NOT NULL,
  `nip` int(30) NOT NULL,
  `soal` varchar(75) NOT NULL,
  `tanggal` date NOT NULL,
  `tampil` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `soal_jwb`
--

CREATE TABLE `soal_jwb` (
  `id_soal` int(5) NOT NULL,
  `nip` int(30) NOT NULL,
  `soal` text NOT NULL,
  `jwb_benar` text,
  `jwb_salah1` text,
  `jwb_salah2` text,
  `jwb_salah3` text,
  `tgl` date NOT NULL,
  `bobot` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_jwb`
--

INSERT INTO `soal_jwb` (`id_soal`, `nip`, `soal`, `jwb_benar`, `jwb_salah1`, `jwb_salah2`, `jwb_salah3`, `tgl`, `bobot`) VALUES
(3, 1, 'Apa yang dimaksud dengan mesin Bubut CNC TU - 2A', 'weq', 'we', 'we', 'we', '2017-11-24', 10),
(4, 1, 'MCU pada mesin gurdi menghasilkan pulsa 20.000 perintah dalam 12 detik untuk mengontrol sebuah motor step sumbu gerakan. jika resolusi kontrol adalah SPD = 0.01 mm, berapakah jarakyang dilintasi dankecepatan sumbu yang bersangkutan (m/min)?', NULL, NULL, NULL, NULL, '2017-12-20', 10),
(9, 1, 'Sebutkan 6 komponen utam dari bagian - bagian utama mesin bubut CNC unit didaktik!', '', '', '', '', '2017-12-29', 30),
(12011, 1, 'Sebuah mesin perkakas digunakan untuk memotong suatu alur dengan sudut 45? sepanjang 50 mm pada bidang XY. Resolusi sistem adalah SPD = 0.01 mm, dan kecepatan pemakanan yang digunakan sepanjang alur = 100 mm/min. Tentukanlah jarak lintasan yang ditempuh (dalam SPD) dan kecepatan (mm/min) dari setiap sumbu.', '', '', '', '', '2018-01-16', 20),
(12012, 1, 'Apakah suatu kontrol loop-terbuka sesuai untuk suatu sistem penggerak pemakanan dari:\na. Meja mesin gurdi yang selalu bergerak pada kecepatan yang sama dan dengan beban yang sama? Mengapa! \nb. Mesin perkakas yang bergerak pada kecepatan yang bervariasi dengan beban yang berbeda? Mengapa!', '', '', '', '', '2018-01-16', 40),
(12013, 1, 'Perencanaan sebuah sistem kontrol loop-terbuka menggunakan 200 step per putaran motor step sebagai perlengkapan penggerak aksial resolusi yang diperlukan adalah SPD = 0.01 mm. \r\na. Berapakah tusuk ulir transportir jika dikopelkan langsung ke motor, di mana jenis ulir tarnsfortir adalah ulir tunggal. \r\nb. Untuk tusuk 5 mm, berapakah perbandingan gigi yang diperlukan antara motor dan transportir?', '', '', '', '', '2018-01-16', 40),
(12021, 1, 'Jelaskan prosedur memasang dan memindahkan posisi sabuk pada puli motor spindel!', '', '', '', '', '2018-01-16', 10),
(12022, 1, 'Berapakah:\na. Jenjang kecepatan pemakanan secara manual\nb. Jenjang kecepatan pemakanan secara CNC', '', '', '', '', '2018-01-16', 40),
(12023, 1, 'Berapakah:\na. Panjang eretan melintang mesin bubut CNC unit didaktik\nb. Panjang eretan memanjang mesin bubut CNC unit didaktik', '', '', '', '', '2018-01-16', 40),
(12024, 1, 'Apakah yang dimaksudkan dengan satuan panjang dasar dan bagaimana hubungannya dengan nilai yang terlihat pada penampil digital?', '', '', '', '', '2018-01-16', 20),
(12025, 1, 'Dapatkah ketebalan atau tebal penyayatan maksimun 0.3 mm harus diterapkan?', '', '', '', '', '2018-01-16', 20),
(12026, 1, 'Jelaskan fungsi dari kepala lepas pada mesin bubut CNC unit didaktik!', '', '', '', '', '2018-01-16', 20),
(12041, 1, 'Sebuah benda kerja berbentuk silindris dengan diameter 80 mm dan panjang 250 mm akan dibubut pada kecepatan potong 200 m/min. Ada dua jenis operasi pembubutan yang diperlukan, yaitu pembubutan kasar pada 0.5 mm/put, dengan kedalaman pemotongan 4 mm dan pembubutan halus (penyelesaian) pada 0.0 mm/put. Tentukanlah kecepatan spindel, kecepatan pemakanan (mm/min), dan waktu pemesinan aktual.', '', '', '', '', '2018-01-16', 20),
(12042, 1, 'Sebutkan pula kriteria yang mempengaruhi kecepatan pemakanan!', '', '', '', '', '2018-01-16', 20),
(12043, 1, 'Apa yang akan terjadi jika kecepatan potong yang dipilih jauh lebih besar dari kecepatan potong yang diperbolehkan, jelaskan!', '', '', '', '', '2018-01-16', 20),
(12051, 1, 'Jelaskan yang dimaksud dengan sistem pemprograman absolut!', '', '', '', '', '2018-01-16', 20),
(12052, 1, 'Jelaskan yang dimaksud dengan sistem pemprograman inkremental!', '', '', '', '', '2018-01-16', 20),
(12053, 1, 'Sebutkan keunggulan masin-masing sistem pemprograman absolut dan sistem pemprograman inkremental!', '', '', '', '', '2018-01-16', 30),
(12054, 1, 'Apakah yang dimaksud dengan G00, dan bilakah digunakan?', '', '', '', '', '2018-01-16', 20),
(12055, 1, 'Bilakah digunakan G01!', '', '', '', '', '2018-01-16', 20),
(12056, 1, 'Apakah perbedaan G02 dengan G03?', '', '', '', '', '2018-01-16', 20),
(12057, 1, 'Dapatkah G00 digunakan untuk penyayatan?', '', '', '', '', '2018-01-16', 20);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `det_jwb_siswa`
--
ALTER TABLE `det_jwb_siswa`
  ADD PRIMARY KEY (`id_det_jwb_siswa`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id_jawaban`),
  ADD KEY `id_soal` (`id_soal`),
  ADD KEY `nis` (`nis`);

--
-- Indexes for table `jawab_siswa`
--
ALTER TABLE `jawab_siswa`
  ADD PRIMARY KEY (`id_jwb_siswa`),
  ADD KEY `nis` (`nis`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `soal_jwb`
--
ALTER TABLE `soal_jwb`
  ADD PRIMARY KEY (`id_soal`),
  ADD KEY `nip` (`nip`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jawaban`
--
ALTER TABLE `jawaban`
  MODIFY `id_jawaban` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD CONSTRAINT `jawaban_ibfk_1` FOREIGN KEY (`id_soal`) REFERENCES `soal_jwb` (`id_soal`) ON UPDATE CASCADE,
  ADD CONSTRAINT `jawaban_ibfk_2` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`) ON UPDATE CASCADE;

--
-- Constraints for table `jawab_siswa`
--
ALTER TABLE `jawab_siswa`
  ADD CONSTRAINT `jawab_siswa_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`);

--
-- Constraints for table `soal_jwb`
--
ALTER TABLE `soal_jwb`
  ADD CONSTRAINT `soal_jwb_ibfk_1` FOREIGN KEY (`nip`) REFERENCES `guru` (`nip`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
