-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2017 at 08:52 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bubut_cnc`
--

-- --------------------------------------------------------

--
-- Table structure for table `det_jwb_siswa`
--

CREATE TABLE `det_jwb_siswa` (
  `id_det_jwb_siswa` int(10) NOT NULL,
  `id_jwb_siswa` int(5) NOT NULL,
  `id_soal` int(5) NOT NULL,
  `id_jawaban` int(5) NOT NULL,
  `jwb_essay` text NOT NULL,
  `bobot_essay` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `nip` int(30) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`nip`, `nama`) VALUES
(1, '1'),
(1213, 'aaaaa'),
(12345, 'Pak Sumpena'),
(23456, 'agung');

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE `jawaban` (
  `id_jawaban` int(5) NOT NULL,
  `id_soal` int(5) DEFAULT NULL,
  `jawaban_` varchar(75) NOT NULL,
  `bobot` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jawaban`
--

INSERT INTO `jawaban` (`id_jawaban`, `id_soal`, `jawaban_`, `bobot`) VALUES
(1, NULL, 'aaaa', NULL),
(2, NULL, 'aaaa', NULL),
(3, NULL, '', NULL),
(4, NULL, '', NULL),
(5, NULL, 'okokoko', NULL),
(6, NULL, 'okokoko', NULL),
(7, NULL, 'lllkkkkk', NULL),
(8, NULL, 'lllkkkkk', NULL),
(9, NULL, 'oekeoeodirjniunvi', NULL),
(10, NULL, 'hahahahahahaha', NULL),
(11, NULL, 'ihihihihihihi', NULL),
(12, NULL, '', NULL),
(13, NULL, '', NULL),
(14, NULL, '', NULL),
(15, NULL, 'kdaksdlkasmdlkasmldakmsl', NULL),
(16, NULL, 'jkllkjlkjljlkjljl', NULL),
(17, NULL, '213123y87hjnkjnkjnkj', NULL),
(18, NULL, 'asjdoiasjdoijsdoijaosijdoaisdasoidascajncsdcs odicidocdsoicnsidvnv', NULL),
(19, NULL, '', NULL),
(20, NULL, 'yang dimaksud dengan mesin bubut ', NULL),
(21, NULL, 'dakmsdklasmdlkamslkdmals', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jawab_siswa`
--

CREATE TABLE `jawab_siswa` (
  `id_jwb_siswa` int(5) NOT NULL,
  `nis` int(10) NOT NULL,
  `nilai` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nis` int(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `kelas` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nis`, `nama`, `kelas`) VALUES
(34242, 'jaya', 'TPM-3');

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE `soal` (
  `id_soal` int(5) NOT NULL,
  `nip` int(30) NOT NULL,
  `soal` varchar(75) NOT NULL,
  `tanggal` date NOT NULL,
  `tampil` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `soal_jwb`
--

CREATE TABLE `soal_jwb` (
  `id_soal` int(5) NOT NULL,
  `nip` int(30) NOT NULL,
  `soal` text NOT NULL,
  `jwb_benar` text,
  `jwb_salah1` text,
  `jwb_salah2` text,
  `jwb_salah3` text,
  `tgl` date NOT NULL,
  `bobot` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal_jwb`
--

INSERT INTO `soal_jwb` (`id_soal`, `nip`, `soal`, `jwb_benar`, `jwb_salah1`, `jwb_salah2`, `jwb_salah3`, `tgl`, `bobot`) VALUES
(3, 1, 'Apa yang dimaksud dengan mesin Bubut CNC TU - 2A', 'weq', 'we', 'we', 'we', '2017-11-24', 10),
(4, 1, 'asdaksdmlkasmdklasnduahsiusafhiufhiu', NULL, NULL, NULL, NULL, '2017-12-20', 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `det_jwb_siswa`
--
ALTER TABLE `det_jwb_siswa`
  ADD PRIMARY KEY (`id_det_jwb_siswa`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id_jawaban`);

--
-- Indexes for table `jawab_siswa`
--
ALTER TABLE `jawab_siswa`
  ADD PRIMARY KEY (`id_jwb_siswa`),
  ADD KEY `nis` (`nis`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `soal`
--
ALTER TABLE `soal`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `soal_jwb`
--
ALTER TABLE `soal_jwb`
  ADD PRIMARY KEY (`id_soal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jawaban`
--
ALTER TABLE `jawaban`
  MODIFY `id_jawaban` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `soal`
--
ALTER TABLE `soal`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `jawab_siswa`
--
ALTER TABLE `jawab_siswa`
  ADD CONSTRAINT `jawab_siswa_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
